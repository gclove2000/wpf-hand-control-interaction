﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using HandyControl.Interactivity;
using HandyControl.Tools.Extension;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows.Documents;
using WpfApp1.Utils;
using WpfApp1.Views;

namespace WpfApp1.ViewModels
{
    public class MainWindowModel : ObservableObject
    {

        public RelayCommand ShowTextDialogBtn => new RelayCommand(async () => await ShowText());

        public RelayCommand ShowSelectDialogBtn => new RelayCommand(async () => await ShowSelect());

        public RelayCommand ShowLoadingDialogBtn => new RelayCommand(()=>DialogHelper.ShowLoading());

        private async Task ShowSelect()
        {
            //var viewModel = new SelectDialogViewModel();
            //var dialog = Dialog.Show(new SelectDialogView())
            //    .Initialize<SelectDialogViewModel>(vm =>
            //    {
            //        viewModel = vm;
            //    });
            //viewModel.Result = string.Empty;
            //dialog.MouseLeftButtonDown += (sender, e) =>
            //{
            //    if(!viewModel.IsClick)
            //    {
            //        dialog.Close();
            //    }
            //};
            //var res = await dialog.GetResultAsync<string>();

            var res = await DialogHelper.ShowSelect(new List<string>() { "选项1", "选项2" }, "请选择");
            if (res == string.Empty)
            {
                Console.WriteLine("窗口关闭");

            }
            else
            {
                Console.WriteLine($"选择了{res}");
            }

        }

        private async Task ShowText()
        {
            //var viewModel = new TextDialogViewModel();
            //var dialog = Dialog.Show<TextDialogView>()
            //    .Initialize<TextDialogViewModel>(vm => {
            //        vm.Content = "";
            //        viewModel = vm;
            //    });
            ////viewModel.CloseAction = dialog.Close;
            //dialog.MouseLeftButtonDown += (sender, e) =>
            //{
            //    if (!viewModel.IsClick)
            //    {
            //        Console.WriteLine("Outside Click!");
            //    }
            //};
            //var res = await dialog
            //.GetResultAsync<TextDialogViewModel.TextDialogResult>();
            var res = await DialogHelper.ShowInput();

            Console.WriteLine(JsonConvert.SerializeObject(res));
        }

        private void Dialog_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Console.WriteLine("Mouse Click");
        }

        public MainWindowModel()
        {

        }
    }
}
