﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Tools.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Views;
using static WpfApp1.ViewModels.TextDialogViewModel;

namespace WpfApp1.ViewModels
{
    public partial class SelectDialogViewModel : ObservableObject, IDialogResultable<string>
    {

        [ObservableProperty]
        private int selectIndex = 0;

        [ObservableProperty]
        private List<string> selectItems = new List<string>() { "Item1", "Item2", "Item3" };


        [ObservableProperty]
        private string title = "请选择";

        public record SelectResult(string selectStr, int selectIndex);

        public RelayCommand<string> ItemSelectBtn => new((msg) =>
        {
            Result = msg;
            CloseAction?.Invoke();
        });

        public bool IsClick { get; set; }


        private SelectDialogView selectDialogView;
        public SelectDialogView SelectDialogView
        {
            get => selectDialogView; set
            {
                selectDialogView = value;
                selectDialogView.MouseLeftButtonDown += (sender, e) =>
                {
                    IsClick = true;
                };
                selectDialogView.MouseLeftButtonUp += (sender, e) =>
                {
                    IsClick = false;
                };
                selectDialogView.MouseLeave += (sender, e) =>
                {
                    IsClick = false;
                };
            }
        }
        public RelayCommand TestBtn => new(() => Console.WriteLine("Hello WPF"));

        public string Result { get; set; } = string.Empty;
        public Action CloseAction { get; set; }

        public SelectDialogViewModel()
        {


        }
    }
}
