﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Views;

namespace WpfApp1.ViewModels
{
    public partial class LoadingDialogViewModel : ObservableObject
    {

        [ObservableProperty]
        private string title = "请稍等";

        public bool IsClick { get; set; }

        private LoadingDialogView loadingDialogView;
        public LoadingDialogView LoadingDialogView
        {
            get => loadingDialogView;
            set
            {
                loadingDialogView = value;
                loadingDialogView.MouseLeftButtonDown += (sender, e) =>
                {
                    IsClick = true;
                };
                loadingDialogView.MouseLeftButtonUp += (sender, e) =>
                {
                    IsClick = false;
                };
                loadingDialogView.MouseLeave += (sender, e) =>
                {
                    IsClick = false;
                };
            }
        }


        public LoadingDialogViewModel()
        {

        }
    }
}
