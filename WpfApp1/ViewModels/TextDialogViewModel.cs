﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Tools.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Views;
using static WpfApp1.ViewModels.TextDialogViewModel;

namespace WpfApp1.ViewModels
{
    public partial class TextDialogViewModel : ObservableObject, IDialogResultable<TextDialogResult>
    {

        public record TextDialogResult(string Str, bool IsConfirm);


        [ObservableProperty]
        private string content = "";


        [ObservableProperty]
        private string confrimText = "确定";

        [ObservableProperty]
        private string cancelText = "取消";

        [ObservableProperty]
        private string title = "标题";

        [ObservableProperty]
        private string placeholder = "请输入文本";

        public bool IsClick { get; set; }

        private TextDialogView textDialogView;

        public bool IsConfirm { get; set; }

        /// <summary>
        /// 拿到TextDialogView的时候，添加点击函数
        /// </summary>
        public TextDialogView TextDialogView
        {
            get => textDialogView;
            set
            {
                textDialogView = value;
                textDialogView.MouseLeftButtonDown += (sender, e) =>
                {
                    IsClick = true;
                };
                textDialogView.MouseLeftButtonUp += (sender, e) =>
                {
                    IsClick = false;
                };
                textDialogView.MouseLeave += (sender, e) =>
                {
                    IsClick = false;
                };
            }
        }

        /// <summary>
        /// 这个接口会给我们关闭的函数
        /// </summary>
        public Action CloseAction { get; set; }

        public RelayCommand ConfirmBtn => new RelayCommand(() =>
        {
            //Console.WriteLine("True");
            CloseAction?.Invoke();
            IsConfirm = true;
        });

        public RelayCommand CancelBtn => new RelayCommand(() =>
        {
            CloseAction?.Invoke();
            IsConfirm = false;

        });


        private TextDialogResult result;

        public TextDialogResult Result
        {
            set
            {
                Content = value.Str;
                IsConfirm = value.IsConfirm;
            }
            get => new TextDialogResult(Content, IsConfirm);
        }



        public TextDialogViewModel()
        {
        
        }




    }
}
