﻿using HandyControl.Controls;
using HandyControl.Tools.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.ViewModels;
using WpfApp1.Views;

namespace WpfApp1.Utils
{
    public class DialogHelper
    {


   

        public static async Task<TextDialogViewModel.TextDialogResult> ShowInput(string confirmText = "确定",
            string cancelText = "取消",
            string title = "标题",
            string placeholder = "请输入文本",
            bool outsideClick = true)
        {
            var viewModel = new TextDialogViewModel();
            var dialog = Dialog.Show<TextDialogView>()
                .Initialize<TextDialogViewModel>(vm =>
                {
                    viewModel = vm;
                });
            viewModel.Title = title;
            viewModel.Placeholder = placeholder;
            viewModel.CancelText = cancelText;
            viewModel.ConfrimText = confirmText;

            //viewModel.CloseAction = dialog.Close;
            dialog.MouseLeftButtonDown += (sender, e) =>
            {
                if (!viewModel.IsClick)
                {
                    if (outsideClick)
                    {
                        dialog.Close();
                    }
                }
            };
            var res = await dialog
            .GetResultAsync<TextDialogViewModel.TextDialogResult>();
            return res;
        }

        public static Dialog ShowLoading(string title = "加载中")
        {
            var viewModel = new LoadingDialogViewModel();
            var dialog = Dialog.Show(new LoadingDialogView())
                .Initialize<LoadingDialogViewModel>(vm =>
                {
                    vm.Title = title;
                });
            dialog.MouseLeftButtonDown += (sender, e) =>
            {
                //Console.WriteLine(viewModel.Title);
                if (!viewModel.IsClick)
                {
                    dialog.Close();
                }

            };
            return dialog;
        }


        public static async Task<string> ShowSelect(List<string> selectItems,string title)
        {
            var viewModel = new SelectDialogViewModel();
            var dialog = Dialog.Show(new SelectDialogView())
                .Initialize<SelectDialogViewModel>(vm =>
                {
                    viewModel = vm;
                });
            viewModel.Result = string.Empty;
            viewModel.SelectItems = selectItems;
            viewModel.Title = title;
            dialog.MouseLeftButtonDown += (sender, e) =>
            {
                if (!viewModel.IsClick)
                {
                    dialog.Close();
                }
            };
            var res = await dialog.GetResultAsync<string>();

            return res;
        }


    }


}
